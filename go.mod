module gitlab.com/gotils/coreaws

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.16
	github.com/google/wire v0.4.0
)
