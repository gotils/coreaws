package coreaws

import (
	"github.com/google/wire"
)

// WireSet ...
var WireSet = wire.NewSet(NewAwsSession)
